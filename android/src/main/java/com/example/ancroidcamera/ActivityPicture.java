package com.example.ancroidcamera;

import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.WritableArray;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


public class ActivityPicture extends AppCompatActivity {

    private boolean isCameraFront = false;
    private boolean isUploadedFromGallery = false;
    private String picturePathUri = "";
    private EditText captionTextInput;
    private int maxLines = 0;
    private Bitmap bitmap, preparedBitmap;
    ImageButton saveImageButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getIntent().getExtras();
        assert bundle != null;
        isCameraFront = bundle.getBoolean("isCameraFront", false);
        isUploadedFromGallery = bundle.getBoolean("isUploadedFromGallery", false);
        picturePathUri = getRealPathFromURI(Uri.parse(bundle.getString("picturePathUri", "")));

        this.initializeCamLayoutSettings();
    }


    public void initializeCamLayoutSettings() {
        setContentView(R.layout.activity_picture);
        Display display = getWindowManager().getDefaultDisplay();
        ImageView imageView = findViewById(R.id.img);
        captionTextInput = findViewById(R.id.captionText);
        ImageButton sendStoryBtn = findViewById(R.id.button2);
        LinearLayout pictureBottomLayout = findViewById(R.id.pictureBottomLayout);
        saveImageButton = findViewById(R.id.saveMedia);
        TextView retakePicBtn = findViewById(R.id.retakePicBtn);
        ViewGroup.MarginLayoutParams marginParams = (ViewGroup.MarginLayoutParams) captionTextInput.getLayoutParams();
        LinearLayout pictureLayout = findViewById(R.id.picture_layout);

        if (isUploadedFromGallery) {
            try {
                bitmap = orienrtateBitmapFromGalerry(MainActivity.bitmap, picturePathUri);
            } catch (IOException e) {
                e.printStackTrace();
            }

            if(checkIsPortraitMode()) {
                preparedBitmap = bitmap.copy(Bitmap.Config.ARGB_8888, true);
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                imageView.setImageBitmap(bitmap);
            } else {
                bitmap = MainActivity.bitmap.copy(Bitmap.Config.ARGB_8888, true);
                //prevent setting new image while save bitmap
                Bitmap previewBitmap = MainActivity.bitmap;
                preparedBitmap = MainActivity.bitmap.copy(Bitmap.Config.ARGB_8888, true);
                imageView.setImageBitmap(previewBitmap);
            }
        } else {
            bitmap = this.rotateBitmapImage(MainActivity.bitmap);
            preparedBitmap = cropBitmapImage(bitmap);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setImageBitmap(bitmap);
        }


        pictureLayout.getViewTreeObserver().addOnGlobalLayoutListener(() -> {
            int heightDiff = pictureLayout.getRootView().getHeight() - pictureLayout.getHeight();
            if (checkIsPortraitMode()) {
                int y = (int) (saveImageButton.getY() - 50);
                if (heightDiff > 250) {
                    marginParams.bottomMargin = display.getHeight() / 4;
                } else {
                    marginParams.height = y;
                }
            } else {
                if (heightDiff > 250) {
                    float[] f = new float[9];
                    imageView.getImageMatrix().getValues(f);
                    final float scaleY = f[Matrix.MSCALE_Y];
                    final Drawable d = imageView.getDrawable();
                    final int origH = d.getIntrinsicHeight();
                    final int activeHeight = Math.round(origH * scaleY);
                    int blackBorder = (display.getHeight() - activeHeight) / 2;
                    marginParams.height = activeHeight + blackBorder - captionTextInput.getLineHeight();
                } else {
                    marginParams.bottomMargin = display.getHeight() / 4;
                }
            }
            captionTextInput.setLayoutParams(marginParams);
        });


        final Handler handler = new Handler();
            handler.postDelayed(() -> {
                float[] f = new float[9];
                imageView.getImageMatrix().getValues(f);
                // Extract the scale values using the constants (if aspect ratio maintained, scaleX == scaleY)
                final float scaleY = f[Matrix.MSCALE_Y];

                // Get the drawable (could also get the bitmap behind the drawable and getWidth/getHeight)
                final Drawable d = imageView.getDrawable();
                final int origH = d.getIntrinsicHeight();
                final int activeHeight = Math.round(origH * scaleY);

                if(isUploadedFromGallery) {
                    int y = (int) (saveImageButton.getY() - 50);
                    maxLines = (y / captionTextInput.getLineHeight()) - 3;
                    marginParams.height = y;

                    int blackBorder = (display.getHeight() - activeHeight) / 2;
                    if (!checkIsPortraitMode()) {
                        maxLines = activeHeight / captionTextInput.getLineHeight();
                        marginParams.height = activeHeight + blackBorder - captionTextInput.getLineHeight();
                    } else {
                        maxLines = (y / captionTextInput.getLineHeight());
                    }
                } else {
                    int y = (int) (saveImageButton.getY() - 50);
                    marginParams.height = y;
                    maxLines = y / captionTextInput.getLineHeight();
                }
                captionTextInput.setLayoutParams(marginParams);
            }, 100);

        captionTextInput.addTextChangedListener(new TextWatcher() {

            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (null != captionTextInput.getLayout() && captionTextInput.getLayout().getLineCount() > maxLines - 1) {
                    captionTextInput.getText().delete(captionTextInput.getText().length() - 1, captionTextInput.getText().length());
                }
            }
        });


        saveImageButton.setOnClickListener(v -> {
            Bitmap finalPreparedBitmap = preparedBitmap;
            runOnUiThread(() -> {
                MainActivity.animateClick(v, 300);
                try {
                    v.setBackgroundResource(R.drawable.saving_circle);
                    v.animate()
                            .withStartAction(() -> saveImage(finalPreparedBitmap, v))
                            .setDuration(20000)
                            .rotation(14400f)
                            .start();
                    v.setEnabled(false);

                    Timer timer = new Timer();
                    timer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            ActivityPicture.this.runOnUiThread(() -> {
                                v.setBackgroundResource(R.drawable.save_icon);
                                v.setEnabled(true);
                            });
                        }
                    }, 10000);

                } catch (Exception e) { }
            });
        });


        sendStoryBtn.setOnClickListener(v -> {

            Thread t = new Thread() {
                @Override
                public void run() {
                    returnMediaDataToReact();
                }
            };

            t.start();
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            //Close application
            if (isUploadedFromGallery) {
                setResult(1);
                finish();
            } else {
                finishAffinity();
            }

        });

        retakePicBtn.setOnClickListener(v -> {
            this.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_BACK));
            this.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_BACK));
        });

        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) pictureBottomLayout.getLayoutParams();
        params.width = (int) (display.getWidth() * 0.9);
        params.leftMargin = (int) (display.getWidth() * 0.05);
    }


    public Boolean saveImage(Bitmap myBitmap, View v) {
        String inputText = captionTextInput.getText().toString();
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        Canvas canvas = new Canvas(myBitmap);
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(Color.rgb(255, 255, 255));
        double imageScale = (myBitmap.getWidth() / 1000.00);
        paint.setTextSize((float) (63 * imageScale));
        paint.setShadowLayer(5, 0, 0, Color.rgb(76, 76, 76));
        paint.setAntiAlias(false);

        int y = (int) (saveImageButton.getY() - 50);
        drawTextAndBreakLine(canvas, paint, myBitmap,  50,
                y, myBitmap.getWidth() - 150, inputText);
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

        File csmDirectoryCache = new File(
                Environment.getExternalStorageDirectory() + MainActivity.CSMDIRECTORY);

        try {
            File f = new File(csmDirectoryCache, "CSPicture_" + Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            f.createNewFile();   //give read write permission
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(this,
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            v.clearAnimation();
            v.setRotation(0);
            v.animate().cancel();
            v.setBackgroundResource(R.drawable.saving_icon);
        } catch (IOException e1) {
            return false;
        }
        return true;
    }

    public File saveImageToCache(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

        File csmDirectoryCache = new File(
                Environment.getExternalStorageDirectory() + MainActivity.CSMDIRECTORY_CACHE);

        try {
            File f = new File(csmDirectoryCache, "CachedPicture_" + Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            f.createNewFile();   //give read write permission
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(this,
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            return f;
        } catch (IOException e1) { }
        return null;
    }


    public void drawTextAndBreakLine(final Canvas canvas, final Paint paint, final Bitmap bitmap,
                                            final float x, final float y, final float maxWidth, final String text) {
        String textToDisplay = text;
        String tempText;
        char[] chars;
        float textHeight = paint.descent() - paint.ascent();
        float lastY;
        int nextPos;
        int lengthBeforeBreak = textToDisplay.length();
        int lineCounter = countDrawLines(paint, maxWidth, text);

        lastY = lineCounter * textHeight;
        int marginBottom = !isUploadedFromGallery ? isCameraFront ? 50 : 200 : 50;
        lastY = bitmap.getHeight() -  lastY - marginBottom;

        do {
            lengthBeforeBreak = textToDisplay.length();
            chars = textToDisplay.toCharArray();
            nextPos = paint.breakText(chars, 0, chars.length, maxWidth, null);
            tempText = textToDisplay.substring(0, nextPos);
            textToDisplay = textToDisplay.substring(nextPos);
            canvas.drawText(tempText, x, lastY, paint);
            lastY += textHeight;
        } while(nextPos < lengthBeforeBreak);
    }

    public int countDrawLines(final Paint paint, final float maxWidth, final String text) {
        String textToDisplay = text;
        char[] chars;
        int nextPos;
        int lengthBeforeBreak = textToDisplay.length();
        int lineCounter = 0;

        //this loop made to detect how much lines do we have
        do {
            lengthBeforeBreak = textToDisplay.length();
            chars = textToDisplay.toCharArray();
            nextPos = paint.breakText(chars, 0, chars.length, maxWidth, null);
            textToDisplay = textToDisplay.substring(nextPos);
            lineCounter++;
        } while (nextPos < lengthBeforeBreak);

        return lineCounter;
    }


        public Bitmap cropBitmapImage(Bitmap myBitmap) {
        Matrix matrix = new Matrix();
        int bitmapWidth = myBitmap.getWidth();
        int bitmapHeight = myBitmap.getHeight();

        if (isUploadedFromGallery) {
            try {
                myBitmap = Bitmap.createBitmap(
                        myBitmap,
                        0,
                        0,
                        bitmapWidth,
                        bitmapHeight
                );
            }
            catch (Exception e) { }
            return myBitmap;
        }

        if (!isCameraFront) {
            myBitmap = Bitmap.createBitmap(
                    myBitmap,
                    0,
                    0,
                    bitmapWidth,
                    bitmapHeight,
                    matrix,
                    false
            );
        } else {
            myBitmap = Bitmap.createBitmap(myBitmap,
                    0,
                    0,
                    bitmapWidth,
                    bitmapHeight,
                    matrix,
                    false
            );
        }

        return myBitmap;
    }

    public Bitmap rotateBitmapImage(Bitmap myBitmap) {
        Matrix matrix = new Matrix();
        matrix.postRotate(90);
        matrix.postScale(1, 1.15f);

        if (isCameraFront) {
            matrix.postRotate(180);
            matrix.postScale(-1.0f, 1.0f);
        }

        myBitmap = Bitmap.createBitmap(myBitmap, 0, 0, myBitmap.getWidth(), myBitmap.getHeight(), matrix, false);

        return myBitmap;
    }

    public void returnMediaDataToReact() {
        String result = MediaStore.Images.Media.insertImage(getContentResolver(), preparedBitmap, "123.jpg", "");
        Log.d("sadasdasd", result);

//        String result = getRealPathFromURI(Uri.fromFile(saveImageToCache(preparedBitmap)));
        int orientation = 0;
        if (!checkIsPortraitMode()) {
            orientation = 1;
        }
        List<String> returnSet = new ArrayList<>();

        //Set first value so that there is a lead value
        returnSet.add(captionTextInput.getText().toString());
        returnSet.add(String.valueOf(0));
        returnSet.add(String.valueOf(Uri.parse(result)));
        returnSet.add(String.valueOf(Uri.parse(result)));
        returnSet.add(String.valueOf(Uri.parse(result)));
        returnSet.add(preparedBitmap != null ? "success" : "fail");
        returnSet.add(String.valueOf(Uri.parse(result)));
        returnSet.add("photo");
        returnSet.add(String.valueOf(preparedBitmap.getWidth()));
        returnSet.add(String.valueOf(preparedBitmap.getHeight()));
        returnSet.add(String.valueOf(orientation));


        String[] returnArray = new String[returnSet.size()];
        returnArray = returnSet.toArray(returnArray);
        WritableArray promiseArray = Arguments.createArray();
        for (String s : returnArray) {
            promiseArray.pushString(s);
        }
        CampusStoryAndroidModule.mCallback.invoke(promiseArray);
    }

    public boolean checkIsPortraitMode() {
        if(!isUploadedFromGallery) {
            return true;
        }

        Bitmap checkedBitmap = null;

        if(preparedBitmap != null) {
            checkedBitmap = preparedBitmap;
        } else {
            checkedBitmap = bitmap;
        }

        if(checkedBitmap == null) {
            try {
                checkedBitmap = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), Uri.parse("file://" + picturePathUri));
            } catch (IOException e) { }
        }

        if (checkedBitmap.getWidth() <= checkedBitmap.getHeight()) {
            return true;
        }
        return false;
    }

    public String getRealPathFromURI(final Uri contentURI) {
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.MediaColumns.DATA);
            if ( idx == -1 ) {
                return contentURI.getPath();
            }
            String rvalue = cursor.getString(idx);
            cursor.close();
            return rvalue;
        }
    }

    public static Bitmap orienrtateBitmapFromGalerry(Bitmap bitmap, String image_absolute_path) throws IOException {
        ExifInterface ei = new ExifInterface(image_absolute_path);
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotate(bitmap, 90);

            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotate(bitmap, 180);

            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotate(bitmap, 270);

            case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                return flip(bitmap, true, false);

            case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                return flip(bitmap, false, true);

            default:
                return bitmap;
        }
    }

    public static Bitmap rotate(Bitmap bitmap, float degrees) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degrees);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    public static Bitmap flip(Bitmap bitmap, boolean horizontal, boolean vertical) {
        Matrix matrix = new Matrix();
        matrix.preScale(horizontal ? -1 : 1, vertical ? -1 : 1);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

}