package com.example.ancroidcamera;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.MediaScannerConnection;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.WritableArray;
import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


public class ActivityVideo extends AppCompatActivity {

    private boolean isCameraFront;
    private EditText captionTextInput;
    public String videoFileName;
    public Boolean isUploadedFromGallery;
    public Uri videoFile;
    public String videoPreview;
    public String croppedVideo, croppedVideoCache, preparedGallerryVideo;
    private VideoView videoView;
    ProgressDialog dialog;
    private Integer maxLines = 0;
    Display display;
    private int videoWidth, videoHeight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getIntent().getExtras();
        assert bundle != null;
        display = getWindowManager().getDefaultDisplay();
        isCameraFront = bundle.getBoolean("isCameraFront", false);
        videoFile = Uri.parse(bundle.getString("videoFile"));


        videoFileName = bundle.getString("videoFileName");
        isUploadedFromGallery = bundle.getBoolean("isUploadedFromGallery", false);
        croppedVideoCache = Environment.getExternalStorageDirectory() + MainActivity.CSMDIRECTORY_CACHE
                + "/" + videoFileName + "_cropped.mp4";

        if (!isUploadedFromGallery) {
            this.startTransformVideo(String.valueOf(videoFile), false, null);
        } else {
            this.preparedGallerryVideo(String.valueOf(videoFile));
        }

        this.initializeCamLayoutSettings();
    }


    public void initializeCamLayoutSettings() {
        setContentView(R.layout.activity_video);
        videoView = findViewById(R.id.videoView);
        LinearLayout videoBottomLayout = findViewById(R.id.videoBottomLayout);
        ImageButton saveVideoButton = findViewById(R.id.saveMedia);
        ImageButton sendStoryBtn = findViewById(R.id.button2);
        captionTextInput = findViewById(R.id.videoCaptionText);
        TextView retakeVideoBtn = findViewById(R.id.retakeVideoBtn);
        LinearLayout videoLayout = findViewById(R.id.video_layout);

        if (!isCameraFront) {
            videoView.setVideoPath(String.valueOf(videoFile));
            videoView.start();
        }

        videoView.setOnCompletionListener(mediaPlayer -> videoView.start());

        videoView.setOnPreparedListener(mp -> {
            videoWidth = mp.getVideoWidth();
            videoHeight = mp.getVideoHeight();
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            int y = (int) (saveVideoButton.getY() - 50);

            if(isUploadedFromGallery) {
                setDimensions(videoWidth, videoHeight);
                int blackBorder = (display.getHeight() - videoView.getHeight()) / 2;
                ViewGroup.MarginLayoutParams marginParams = (ViewGroup.MarginLayoutParams) captionTextInput.getLayoutParams();

                if (videoWidth < videoHeight) {
                    marginParams.height = y;
                    maxLines = y / captionTextInput.getLineHeight();
                    params.addRule(RelativeLayout.ABOVE, R.id.videoBottomLayout);
                    params.addRule(RelativeLayout.ALIGN_PARENT_START, RelativeLayout.TRUE);
                    params.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
                    params.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
                    params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
                    params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
                    videoView.setLayoutParams(params);
                    captionTextInput.setLayoutParams(marginParams);
                } else {
                    maxLines = (videoHeight / captionTextInput.getLineHeight()) / 2;
                    marginParams.height = videoView.getHeight() + blackBorder + captionTextInput.getLineHeight();
                    captionTextInput.setLayoutParams(marginParams);
                }
                captionTextInput.setLayoutParams(marginParams);
            } else {
                params.addRule(RelativeLayout.ABOVE, R.id.videoBottomLayout);
                params.addRule(RelativeLayout.ALIGN_PARENT_START, RelativeLayout.TRUE);
                params.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
                params.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
                params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
                params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
                videoView.setLayoutParams(params);
                setDimensions(display.getWidth(), display.getHeight());

                maxLines = y / captionTextInput.getLineHeight();
                ViewGroup.MarginLayoutParams captionTextInputLayoutParams = (ViewGroup.MarginLayoutParams) captionTextInput.getLayoutParams();
                captionTextInputLayoutParams.bottomMargin = 20;
            }
        });

        videoLayout.getViewTreeObserver().addOnGlobalLayoutListener(() -> {
            ViewGroup.MarginLayoutParams marginParams = (ViewGroup.MarginLayoutParams) captionTextInput.getLayoutParams();
            int heightDiff = videoLayout.getRootView().getHeight() - videoLayout.getHeight();
            int y = (int) (saveVideoButton.getY() - 50);

            if (videoWidth < videoHeight) {
                if (heightDiff > 250) {
                    marginParams.height = y;
                } else {
                    marginParams.height = y;
                }
            } else {
                if (heightDiff > 250) {
                    marginParams.bottomMargin = display.getHeight() / 4;
                }
            }
            captionTextInput.setLayoutParams(marginParams);
        });


        captionTextInput.addTextChangedListener(new TextWatcher() {

            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (null != captionTextInput.getLayout() && captionTextInput.getLayout().getLineCount() > maxLines - 1) {
                    captionTextInput.getText().delete(captionTextInput.getText().length() - 1, captionTextInput.getText().length());
                }
            }
        });

        saveVideoButton.setOnClickListener(v -> runOnUiThread(() -> {
            String videoToTransform;
            if(isUploadedFromGallery) {
                videoToTransform = this.getRealPathFromURI(videoFile);
            } else {
                videoToTransform = String.valueOf(Uri.parse(String.valueOf(videoFile)));
            }
            MainActivity.animateClick(v, 300);
            try {
                v.setBackgroundResource(R.drawable.saving_circle);
                v.animate()
                        .withStartAction(() -> this.startTransformVideo(String.valueOf(videoToTransform), true, v))
                        .setDuration(20000)
                        .rotation(14400f)
                        .start();
                v.setEnabled(false);
            } catch (Exception e) {
            }
        }));

        sendStoryBtn.setOnClickListener(v -> {

            Thread t = new Thread() {
                @Override
                public void run() {
                    try {
                        String[] data = new String[0];
                        data = returnMediaDataToReact();

                        try {
                            List<String> returnSet = new ArrayList<>();
                            //Set first value so that there is a lead value
                            returnSet.add(data[0]);
                            returnSet.add(data[1]);
                            returnSet.add(data[2]);
                            returnSet.add(data[3]);
                            returnSet.add(data[4]);
                            returnSet.add(data[5]);
                            returnSet.add(data[6]);
                            returnSet.add(data[7]);
                            returnSet.add(data[8]);
                            returnSet.add(data[9]);
                            returnSet.add(data[10]);
                            String[] returnArray = new String[returnSet.size()];
                            returnArray = returnSet.toArray(returnArray);
                            WritableArray promiseArray = Arguments.createArray();
                            for (int i = 0; i < returnArray.length; i++) {
                                promiseArray.pushString(returnArray[i]);
                            }
                            CampusStoryAndroidModule.mCallback.invoke(promiseArray);
                        } catch (Exception e) {
                        }
                    } catch (Exception e) {
                    }
                }
            };

            t.start();
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            //Close application
            finishAffinity();
        });

        retakeVideoBtn.setOnClickListener(v -> finish());


        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) videoBottomLayout.getLayoutParams();
        params.width = (int) (display.getWidth() * 0.9);
        params.leftMargin = (int) (display.getWidth() * 0.05);
    }

    public Bitmap createVideoThumbnail(Uri video) {
        Bitmap previewPicture = null;
        if (isUploadedFromGallery) {
            try {
                String picPath = getRealPathFromURI(video);
                previewPicture = ThumbnailUtils.createVideoThumbnail(picPath,
                        MediaStore.Video.Thumbnails.MINI_KIND);
            } catch (Exception e) { }

        } else {
            previewPicture = ThumbnailUtils.createVideoThumbnail(String.valueOf(video),
                    MediaStore.Video.Thumbnails.MINI_KIND);
        }

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        previewPicture.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        File csmDirectoryCache = new File(
                Environment.getExternalStorageDirectory() + MainActivity.CSMDIRECTORY_CACHE);

        try {
            File f = new File(csmDirectoryCache, "preview_" + videoFileName + ".jpg");
            f.createNewFile();   //give read write permission
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(this,
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            videoPreview = Environment.getExternalStorageDirectory() + MainActivity.CSMDIRECTORY_CACHE + "/" + "preview_" + videoFileName + ".jpg";
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return previewPicture;
    }

    public String getRealPathFromURI(final Uri contentURI) {
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.MediaColumns.DATA);
            if ( idx == -1 ) {
                return contentURI.getPath();
            }
            String rvalue = cursor.getString(idx);
            cursor.close();
            return rvalue;
        }
    }

    private void startTransformVideo(String filePath, Boolean saveVideo, View v) {
        croppedVideo = Environment.getExternalStorageDirectory() + MainActivity.CSMDIRECTORY
                + "/" + videoFileName + Calendar.getInstance()
                .getTimeInMillis() + ".mp4";

        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        retriever.setDataSource(filePath);

        int width = Integer.valueOf(retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH));
        float height = Integer.valueOf(retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT));
        float videoScale = height / display.getWidth();
        retriever.release();

        String captionText = saveVideo && !captionTextInput.getText().toString().equals("") ?
                captionTextInput.getText().toString() :
                "";

        String textDraw = isCameraFront ? "hflip," : "";

        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setTextSize(22 * Resources.getSystem().getDisplayMetrics().density * videoScale);
        paint.setAntiAlias(false);

        MediaPlayer mpl = MediaPlayer.create(this, Uri.parse(filePath));
        int videoCalculatedWidth = (int) (mpl.getVideoWidth() * 0.8);

        List<String> captionTextReversed = breakTextLines(paint, videoCalculatedWidth, mpl.getVideoHeight(), captionText);

        float y = captionTextReversed.size() * (30 * Resources.getSystem().getDisplayMetrics().density * videoScale);
        int marginBottom = !isUploadedFromGallery ? 200 : 50;
        y = mpl.getVideoHeight() - y - marginBottom;

        for (String line : captionTextReversed) {
            textDraw += "drawtext=fontsize=" + 22 * Resources.getSystem().getDisplayMetrics().density * videoScale + ":fontfile=/system/fonts/Roboto-Bold.ttf:fontcolor=white:shadowcolor=gray:shadowx=2:shadowy=2:text='" + line + "':x=" + width * 0.05 + ":y=" + y + ",";
            y += 30 * Resources.getSystem().getDisplayMetrics().density * videoScale;
        }

        textDraw = textDraw.substring(0, textDraw.length() - 1);

        String[] cmd = {
                "-i",
                filePath,
                "-s",
                "720x1080",
                "-vf",
                textDraw,
                "-c",
                "copy",
                "-c:v",
                "libx264",
                "-b:v",
                "2M",
                "-maxrate",
                "4M",
                "-bufsize",
                "4M",
                "-profile:v",
                "baseline",
                "-y",
                "-preset",
                "ultrafast",
                saveVideo ? croppedVideo : croppedVideoCache
        };


        String[] commandVideoPreview = {
                "-i",
                filePath,
                "-vf",
                isCameraFront
                        ? "hflip"
                        : "drawtext=fontsize=0:fontfile=/system/fonts/Roboto-Bold.ttf:fontcolor=white:text=:x=50:y=h*0.7",
                "-c",
                "copy",
                "-c:v",
                "libx264",
                "-b:v",
                "2M",
                "-maxrate",
                "4M",
                "-bufsize",
                "4M",
                "-preset",
                "ultrafast",
                Environment.getExternalStorageDirectory() + MainActivity.CSMDIRECTORY_CACHE
                        + "/" + videoFileName + "_preview.mp4"
        };
        executeFFmpegCmd(cmd, commandVideoPreview, saveVideo, v);
    }

    private void executeFFmpegCmd(String[] cmd, String[] commandVideoPreview, Boolean saveVideo, View v) {
        dialog = new ProgressDialog(ActivityVideo.this);
        dialog.setMessage("Processing video...");
        dialog.setCancelable(false);
        dialog.setInverseBackgroundForced(false);

        FFmpeg ffmpeg = FFmpeg.getInstance(this);
        try {
            //Load the binary
            ffmpeg.loadBinary(new LoadBinaryResponseHandler() {
                @Override
                public void onStart() {
                }

                @Override
                public void onFailure() {
                }

                @Override
                public void onSuccess() {
                }

                @Override
                public void onFinish() {
                }
            });
        } catch (FFmpegNotSupportedException e) {
            // Handle if FFmpeg is not supported by device
        }

        if (isCameraFront && !saveVideo) {
            try {
                ffmpeg.execute(commandVideoPreview, new ExecuteBinaryResponseHandler() {

                    @Override
                    public void onStart() {
                    }

                    @Override
                    public void onProgress(String message) {
                    }

                    @Override
                    public void onFailure(String message) {
                    }

                    @Override
                    public void onSuccess(String message) {
                    }

                    @Override
                    public void onFinish() {
                        videoView.setVideoPath(Environment.getExternalStorageDirectory() + MainActivity.CSMDIRECTORY_CACHE
                                + "/" + videoFileName + "_preview.mp4");
                        videoView.start();
                    }
                });
            } catch (FFmpegCommandAlreadyRunningException e) {
            }
        }


        try {
            ffmpeg.execute(cmd, new ExecuteBinaryResponseHandler() {

                @Override
                public void onStart() {
                    if (!saveVideo) {
                        dialog.show();
                    }
                }

                @Override
                public void onProgress(String message) {
                    Log.d("ffmpgDBG", message);
                }

                @Override
                public void onFailure(String message) {
                    Log.d("ffmpgDBG", message);
                }

                @Override
                public void onSuccess(String message) {
                    Log.d("ffmpgDBG", message);
                }

                @Override
                public void onFinish() {
                    if (!saveVideo) {
                        videoView.start();
                        dialog.hide();
                    } else {
                        v.clearAnimation();
                        v.setRotation(0);
                        v.animate().cancel();
                        v.setBackgroundResource(R.drawable.saving_icon);

                        Timer timer = new Timer();
                        timer.schedule(new TimerTask() {
                            @Override
                            public void run() {
                                ActivityVideo.this.runOnUiThread(() -> {
                                    v.setBackgroundResource(R.drawable.save_icon);
                                    v.setEnabled(true);
                                });
                            }
                        }, 10000);
                    }
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
        }
    }

    private void preparedGallerryVideo(String filePath) {

        filePath = "file://" + getRealPathFromURI(Uri.parse(filePath));
        preparedGallerryVideo = Environment.getExternalStorageDirectory() + MainActivity.CSMDIRECTORY_CACHE
                + "/" + videoFileName;

        String[] command = {
                "-i",
                filePath,
                "-s",
                "720x1080",
                "-r",
                "30",
                "-c",
                "copy",
                "-c:v",
                "libx264",
                "-b:v",
                "1M",
                "-maxrate",
                "2M",
                "-bufsize",
                "2M",
                "-profile:v",
                "baseline",
                "-y",
                "-preset",
                "ultrafast",
                preparedGallerryVideo
        };


        dialog = new ProgressDialog(ActivityVideo.this);
        dialog.setMessage("Processing video...");
        dialog.setCancelable(false);
        dialog.setInverseBackgroundForced(false);

        FFmpeg ffmpeg = FFmpeg.getInstance(this);


        try {
            ffmpeg.execute(command, new ExecuteBinaryResponseHandler() {

                @Override
                public void onStart() {
                    dialog.show();
                }

                @Override
                public void onProgress(String message) {
                    Log.d("ffmpgDBG", message);
                }

                @Override
                public void onFailure(String message) {
                    Log.d("ffmpgDBG", message);
                }

                @Override
                public void onSuccess(String message) {
                    Log.d("ffmpgDBG", message);
                }

                @Override
                public void onFinish() {
                    dialog.hide();
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
        }
    }


    public String[] returnMediaDataToReact() throws FileNotFoundException {
        Uri video = null;
        Uri videoCompressedUri = null;
        File compressedFileExistCheck;

        if (isUploadedFromGallery) {
            video = videoFile;
        } else {
            video = Uri.parse(croppedVideoCache);
        }
        MediaPlayer mpl = MediaPlayer.create(this, video);
        createVideoThumbnail(video);
        ContentValues values = new ContentValues(3);
        values.put(MediaStore.Video.Media.MIME_TYPE, "video/mp4");

        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        retriever.setDataSource(getRealPathFromURI(video));
        int width = Integer.valueOf(retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH));
        int height = Integer.valueOf(retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT));

        if (isUploadedFromGallery) {
            compressedFileExistCheck = new File(getRealPathFromURI(video));
            videoCompressedUri = Uri.parse("file://" + preparedGallerryVideo);
        } else {
            values.put(MediaStore.Video.Media.DATA, String.valueOf(video));
            values.put(MediaStore.Video.Media.TITLE, videoFileName + "_cropped.mp4");
            videoCompressedUri = getContentResolver().insert(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, values);
            compressedFileExistCheck = new File(String.valueOf(video));
        }



        int orientation = 0;

        if (isUploadedFromGallery) {
            if (Integer.parseInt(retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_ROTATION)) == 0) {
                orientation = 1;
            }
        }


        int videoDuration = mpl.getDuration() / 1000;
        String videoPreviewResult = MediaStore.Images.Media.insertImage(getContentResolver(), videoPreview, "", "");

        return new String[]{
                captionTextInput.getText().toString(),
                String.valueOf(videoDuration),
                String.valueOf(videoCompressedUri),
                String.valueOf(videoCompressedUri),
                String.valueOf(videoCompressedUri),
                compressedFileExistCheck.exists() ? "success" : "fail",
                videoPreviewResult,
                "video",
                String.valueOf(width),
                String.valueOf(height),
                String.valueOf(orientation)
        };
    }

    public ArrayList<String> breakTextLines(final Paint paint, final float maxWidth, final float videoHeight, final String text) {
        String textToDisplay = text;
        String tempText = "";
        ArrayList<String> linesArray = new ArrayList<>();
        int lineCounter = countDrawLines(paint, maxWidth, text);
        float textHeight = paint.descent() - paint.ascent();
        int lastY = (int) (lineCounter * textHeight);
        int marginBottom = !isUploadedFromGallery ? 200 : 50;
        lastY = (int) (videoHeight -  lastY - marginBottom);

        char[] chars;
        int nextPos = 0;
        int lengthBeforeBreak = textToDisplay.length();
        do {
            lengthBeforeBreak = textToDisplay.length();
            chars = textToDisplay.toCharArray();
            nextPos = paint.breakText(chars, 0, chars.length, maxWidth, null);
            tempText = textToDisplay.substring(0, nextPos);
            textToDisplay = textToDisplay.substring(nextPos);
            linesArray.add(tempText);
        } while(nextPos < lengthBeforeBreak);

        return linesArray;
    }

    public int countDrawLines(final Paint paint, final float maxWidth, final String text) {
        String textToDisplay = text;
        char[] chars;
        int nextPos;
        int lengthBeforeBreak = textToDisplay.length();
        int lineCounter = 0;

        //this loop made to detect how much lines do we have
        do {
            lengthBeforeBreak = textToDisplay.length();
            chars = textToDisplay.toCharArray();
            nextPos = paint.breakText(chars, 0, chars.length, maxWidth, null);
            textToDisplay = textToDisplay.substring(nextPos);
            lineCounter++;
        } while (nextPos < lengthBeforeBreak);

        return lineCounter;
    }

    public void setDimensions(int w, int h) {
        videoView.getLayoutParams().width = w;
        videoView.getLayoutParams().height = h;
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (!isUploadedFromGallery) {
           dialog.dismiss();
        }
        videoView.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        videoView.start();
    }
}