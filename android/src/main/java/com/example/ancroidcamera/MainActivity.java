package com.example.ancroidcamera;


import android.Manifest;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import static android.hardware.Camera.Parameters.FOCUS_MODE_AUTO;

public class MainActivity extends AppCompatActivity {

    public static final String CSMDIRECTORY_CACHE = "/CampusStories/CACHE";
    public static final String CSMDIRECTORY = "/CampusStories";
    private Camera mCamera;
    private MediaRecorder mediaRecorder;
    private Camera.Parameters mParams;
    private Camera.Parameters parametersAdditional;
    private CameraPreview mPreview;
    private Camera.PictureCallback mPicture;
    private ImageButton switchCamera, capture, toggleFlash, importMedia;
    private TextView videoTimer;
    private Context myContext;
    private LinearLayout videoTimerLayout;
    private RelativeLayout emulatedFrontFlash;
    private int cameraId;
    private boolean cameraFront;
    private boolean enabledFlashLight = false;
    public static Bitmap bitmap;
    protected CountDownTimer countDownTimer, countDownTimer1;
    private File videoFile;
    private String videoFileName;
    private TextView cancelAppBtn;
    private Camera.Size mSize;
    private int currentZoom;
    private static final int CLICK_ANIMATION_DELAY = 300;
    private int increaseZoom;
    private int maxZoom;
    private ScaleGestureDetector mScaleGestureDetector;
    public static final int PICK_IMAGE = 1;
    private Animation animScalePlus, animScaleMinus;


    public boolean isCameraFront() {
        return cameraFront;
    }

    public void setCameraFront(boolean cameraFront) {
        this.cameraFront = cameraFront;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.checkNeedlePermissions();

        File csmDirectory = new File(
                Environment.getExternalStorageDirectory() + CSMDIRECTORY);
        File csmDirectoryCache = new File(
                Environment.getExternalStorageDirectory() + CSMDIRECTORY_CACHE);
        // have the object build the directory structure, if needed.
        if (!csmDirectory.exists()) {
            csmDirectory.mkdirs();
        }

        if (csmDirectoryCache.exists()) {
            try {
                deleteRecursive(csmDirectoryCache);
            } finally {
                csmDirectoryCache.mkdirs();
            }
        } else {
            csmDirectoryCache.mkdirs();
        }

        videoFileName = "CSVideo_" + Calendar.getInstance().getTimeInMillis();
        videoFile = new File(csmDirectoryCache, videoFileName + ".mp4");


        this.initializeCamLayoutSettings();
        this.initializeCamera();
        this.checkAvaliableFlash();
        this.setCameraDisplayOrientation();

        mCamera.startPreview();
    }


    @SuppressLint("ClickableViewAccessibility")
    public void initializeCamLayoutSettings() {

        setContentView(R.layout.activity_main);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN); //enable full screen
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        myContext = getApplicationContext();
        LinearLayout cameraPreview = findViewById(R.id.cPreview);
        LinearLayout centerBottomLayout = findViewById(R.id.centerBottomLayout);
        capture = findViewById(R.id.btnCam);
        switchCamera = findViewById(R.id.btnSwitchCam);
        videoTimer = findViewById(R.id.videoTimer);
        videoTimerLayout = findViewById(R.id.videoTimerLayout);
        emulatedFrontFlash = findViewById(R.id.emulatedFrontFlash);
        emulatedFrontFlash.setAlpha(0);
        emulatedFrontFlash.bringToFront();

        Display display = getWindowManager().getDefaultDisplay();
        centerBottomLayout.getLayoutParams().height = display.getHeight() / 3; // aspect ratio 3:2

        mPreview = new CameraPreview(myContext, mCamera);
        cameraPreview.addView(mPreview);

        final GestureDetector gd = new GestureDetector(myContext, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onDoubleTap(MotionEvent e) {
                if (mediaRecorder != null) {
                    return false;
                }

                int camerasNumber = Camera.getNumberOfCameras();
                if (camerasNumber > 1) {
                    releaseCamera();
                    chooseCamera();
                }
                return true;
            }

            @Override
            public void onLongPress(MotionEvent e) {
                super.onLongPress(e);
            }

            @Override
            public boolean onDoubleTapEvent(MotionEvent e) {
                return true;
            }

            @Override
            public boolean onDown(MotionEvent e) {
                return true;
            }

            @Override
            public boolean onSingleTapConfirmed(MotionEvent e) {
                focusOnTouch();
                return super.onSingleTapConfirmed(e);
            }
        });


        capture.setOnClickListener(v -> {
            animateClick(v, CLICK_ANIMATION_DELAY);
            try {
                if (mediaRecorder != null) {
                    mediaRecorder.stop();
                    releaseMediaRecorder();
                } else {
                    if (isCameraFront() && enabledFlashLight && !this.checkAvaliableFlash()) {
                        emulatedFrontFlash.setAlpha(1);
                    }
                    mCamera.takePicture(null, null, mPicture);
                }
            } catch (Exception e) { }
        });


        capture.setOnLongClickListener(v -> {
            if (prepareVideoRecorder()) {
                mediaRecorder.start();
                this.isEnabledFlashLight();
                videoTimerLayout.setVisibility(View.VISIBLE);
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else {
                releaseMediaRecorder();
            }

            return false;
        });

        capture.setOnTouchListener((v, event) -> {
            switch(event.getAction()){
                case MotionEvent.ACTION_UP:
                    if(mediaRecorder != null) {
                        try {
                            Thread.sleep(500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        runOnUiThread(this::releaseMediaRecorder);
                    }
                    break;
            }
            return false;
        });


        switchCamera.setOnClickListener(v -> {
            animateClick(v, CLICK_ANIMATION_DELAY);
            int camerasNumber = Camera.getNumberOfCameras();
            if (camerasNumber > 1) {
                releaseCamera();
                chooseCamera();
            }
        });


        mScaleGestureDetector = new ScaleGestureDetector(this, new ScaleGestureDetector.OnScaleGestureListener() {
            @Override
            public void onScaleEnd(ScaleGestureDetector detector) {
            }

            @Override
            public boolean onScaleBegin(ScaleGestureDetector detector) {
                return true;
            }

            @Override
            public boolean onScale(ScaleGestureDetector detector) {
                if (detector.getPreviousSpan() < detector.getCurrentSpan()) {
                    try {
                        Thread.sleep(15);
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                    gestureZoom(true);
                } else if (detector.getPreviousSpan() == detector.getCurrentSpan()) {
                    return false;
                } else {
                    gestureZoom(false);
                }
                return false;
            }
        });

        cameraPreview.setOnTouchListener((view, motionEvent) -> {
            mScaleGestureDetector.onTouchEvent(motionEvent);
            gd.onTouchEvent(motionEvent);
            return true;
        });

        switchCamera.setOnClickListener(v -> {
            animateClick(v, CLICK_ANIMATION_DELAY);
            int camerasNumber = Camera.getNumberOfCameras();
            if (camerasNumber > 1) {
                releaseCamera();
                chooseCamera();
            }
        });

        toggleFlash = findViewById(R.id.toggleFlash);
        toggleFlash.setOnClickListener(v -> {
            animateClick(v, CLICK_ANIMATION_DELAY);
            enabledFlashLight = !enabledFlashLight;
            cameraFlashMode(enabledFlashLight);
        });

        importMedia = findViewById(R.id.importMedia);
        importMedia.setOnClickListener(v -> {
            animateClick(v, CLICK_ANIMATION_DELAY);


            Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            pickIntent.setType("image/* video/*");
            pickIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 15);
            startActivityForResult(pickIntent, PICK_IMAGE);
        });

        cancelAppBtn = findViewById(R.id.cancelAppBtn);
        cancelAppBtn.setOnClickListener(v -> finishAffinity());
    }

    private boolean focusOnTouch() {

        List<String> supportedFocusModes = mCamera.getParameters().getSupportedFocusModes();
        boolean hasAutoFocus = supportedFocusModes != null && supportedFocusModes.contains(Camera.Parameters.FOCUS_MODE_AUTO);

        if (!hasAutoFocus) {
            return false;
        }

        if (mCamera != null) {

            if (mParams.getMaxNumMeteringAreas() > 0) {
                mParams.setFocusMode(FOCUS_MODE_AUTO);
                mCamera.setParameters(mParams);
                mCamera.autoFocus(mAutoFocusTakePictureCallback);
            } else {
                mCamera.autoFocus(mAutoFocusTakePictureCallback);
            }
        }
        return true;
    }

    public static void animateClick(View v, int Time) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(v, View.ALPHA, 0, 1);
        animator.setDuration(Time);
        animator.start();
    }

    private Camera.AutoFocusCallback mAutoFocusTakePictureCallback = (success, camera) -> {
        if (success) {
            Log.i("tap_to_focus", "success!");
        } else {
            // do something...
            Log.i("tap_to_focus", "fail!");
        }
    };


    public void initializeCamera() {
        cameraId = findBackFacingCamera();
        if (cameraId >= 0) {
            mCamera = Camera.open(cameraId);

            Camera.Parameters params = mCamera.getParameters();
            if (params.getSupportedFocusModes().contains(
                    Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO)) {
                params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO);
                mParams = params;
            }

            mPicture = getPictureCallback();
            mPreview.refreshCamera(mCamera);
            this.cameraFlashMode(enabledFlashLight);

            mSize = getPictureSize(false);

            if (mSize == null) {
                mSize = getPictureSize(true);
            }

            params.setPictureSize(Objects.requireNonNull(mSize).width, mSize.height);


            mCamera.setParameters(params);
        }
    }

    public void gestureZoom(Boolean isPinch) {
        if (!isCameraFront()) {
            parametersAdditional = mParams;
        }

        if (parametersAdditional == null) {
            parametersAdditional = mCamera.getParameters();
        }

        increaseZoom = parametersAdditional.getMaxZoom() / 25;
        maxZoom = parametersAdditional.getMaxZoom();

        currentZoom = parametersAdditional.getZoom();

        if (parametersAdditional.isZoomSupported()) {
            if (isPinch) {
                if (currentZoom + increaseZoom <= maxZoom) {
                    parametersAdditional.setZoom(currentZoom + increaseZoom);
                }
            } else {
                if (currentZoom - increaseZoom >= 0) {
                    parametersAdditional.setZoom(currentZoom - increaseZoom);
                }
            }
        }

        mCamera.setParameters(parametersAdditional);
    }


    public Camera.Size getPictureSize(Boolean getMaxSize) {
        List<Camera.Size> sizes = mParams.getSupportedPictureSizes();
        Camera.Size mSize = null;
        Camera.Size biggestSize = sizes.get(0);
        for (Camera.Size size : sizes) {
            if (!getMaxSize) {
                if (size.width == 1920 && size.height == 1080) {
                    mSize = size;
                    break;
                }
            } else {
                if (biggestSize.width < size.width) {
                    mSize = size;
                } else {
                    mSize = biggestSize;
                }
            }
        }
        return mSize;
    }


    public void cameraFlashMode(Boolean enabledFlashLight) {

        Camera.Parameters parameters = mCamera.getParameters();

        if (parameters.getFlashMode() == null) {
            return;
        }

        if (enabledFlashLight) {
            if (mCamera != null) {
                if (mParams != null) {
                    List<String> supportedFlashModes = mParams.getSupportedFlashModes();

                    if (supportedFlashModes != null) {
                        if (supportedFlashModes.contains(Camera.Parameters.FLASH_MODE_ON)) {
                            toggleFlash.setBackgroundResource(R.drawable.flash_on);
                            mParams.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
                        } else {
                            toggleFlash.setBackgroundResource(R.drawable.flash_off);
                            mCamera = null;
                        }
                    } else Log.d("Camera flash: ", "Camera is null.");
                }
            }
        } else {
            toggleFlash.setBackgroundResource(R.drawable.flash_off);
            mParams.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
        }
        assert mCamera != null;
        mCamera.setParameters(mParams);
    }


    private void checkNeedlePermissions() {
        ActivityCompat.requestPermissions(MainActivity.this, new String[]{
                android.Manifest.permission.CAMERA,
                Manifest.permission.RECORD_AUDIO,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
        }, 37);
    }

    private int findFrontFacingCamera() {

        int cameraId = -1;
        // Search for the front facing camera
        int numberOfCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                cameraId = i;
                break;
            }
        }
        return cameraId;
    }

    private int findBackFacingCamera() {
        int cameraId = -1;
        //Search for the back facing camera
        //get the number of cameras
        int numberOfCameras = Camera.getNumberOfCameras();
        //for every camera check
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                cameraId = i;
                break;
            }
        }
        return cameraId;
    }

    public void onResume() {
        super.onResume();
        if (mCamera == null) {
            mCamera = Camera.open();
            this.setCameraDisplayOrientation();
            mCamera.setParameters(mParams);
            this.setCameraFront(false);
            this.checkAvaliableFlash();
            mPicture = getPictureCallback();
            mPreview.refreshCamera(mCamera);
            this.isEnabledFlashLight();
            emulatedFrontFlash.setAlpha(0);
        }
    }

    public void chooseCamera() {
        //if the camera preview is the front
        if (cameraFront) {
            cameraId = findBackFacingCamera();
            if (cameraId >= 0) {
                //open the backFacingCamera
                //set a picture callback
                //refresh the preview
                this.setCameraFront(false);
//                cameraFront = false;
                mCamera = Camera.open(cameraId);
                this.setCameraDisplayOrientation();
                mCamera.setParameters(mParams);
                mPicture = getPictureCallback();
                mPreview.refreshCamera(mCamera);
            }
        } else {
            cameraId = findFrontFacingCamera();
            if (cameraId >= 0) {
                //open the backFacingCamera
                //set a picture callback
                //refresh the preview
                this.setCameraFront(true);
//                cameraFront = true;
                mCamera = Camera.open(cameraId);
                this.setCameraDisplayOrientation();
                mPicture = getPictureCallback();
                mPreview.refreshCamera(mCamera);
            }
        }
        parametersAdditional = mCamera.getParameters();
        mParams.setZoom(0);
        this.checkAvaliableFlash();
        this.isEnabledFlashLight();
    }

    public boolean checkAvaliableFlash() {
        Camera.Parameters parameters = mCamera.getParameters();

        if (parameters.getFlashMode() != null) {
            return true;
        }
        return false;
    }

    public void isEnabledFlashLight() {
        if (!this.checkAvaliableFlash()) {
            return;
        }

        List<String> supportedFlashModes = mParams.getSupportedFlashModes();
        if (mediaRecorder != null && enabledFlashLight) {
            if (supportedFlashModes.contains(Camera.Parameters.FLASH_MODE_TORCH)) {
                mParams.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                toggleFlash.setBackgroundResource(R.drawable.flash_off);
                enabledFlashLight = true;
            } else {
                toggleFlash.setBackgroundResource(R.drawable.flash_off);
                enabledFlashLight = false;
                mCamera = null;
            }
        } else {
            if (enabledFlashLight) {
                if (supportedFlashModes.contains(Camera.Parameters.FLASH_MODE_ON)) {
                    mParams.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
                    toggleFlash.setBackgroundResource(R.drawable.flash_off);
                    enabledFlashLight = true;
                } else {
                    toggleFlash.setBackgroundResource(R.drawable.flash_off);
                    mCamera = null;
                    enabledFlashLight = false;
                }
            } else {
                mParams.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                enabledFlashLight = false;
            }
        }

        assert mCamera != null;
        mCamera.setParameters(mParams);
    }


    public void setCameraDisplayOrientation() {
        Camera.CameraInfo camInfo = new Camera.CameraInfo();
        Camera.getCameraInfo(cameraId, camInfo);

        Display display = ((WindowManager) myContext.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();

        int rotation = display.getRotation();
        int degrees = 0;

        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }

        int result;

        if (camInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (camInfo.orientation + degrees) % 360;
            result = (360 - result) % 360;  // compensate the mirror
        } else {  // back-facing
            result = (camInfo.orientation - degrees + 360) % 360;
        }

        mCamera.setDisplayOrientation(result);
    }

    @Override
    protected void onPause() {
        super.onPause();
        //when on Pause, release camera in order to be used from other applications
        releaseCamera();
    }

    private void releaseCamera() {
        // stop and release camera
        if (mCamera != null) {
            mCamera.stopPreview();
            mCamera.setPreviewCallback(null);
            mCamera.release();
            mCamera = null;
        }
    }

    private Camera.PictureCallback getPictureCallback() {
        return (data, camera) -> {
            bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);

            Intent intent = new Intent(MainActivity.this, ActivityPicture.class);
            Bundle bundle = new Bundle();
            bundle.putBoolean("isCameraFront", isCameraFront());
            intent.putExtras(bundle);
            startActivity(intent);
        };
    }


    private boolean prepareVideoRecorder() {
        if(mediaRecorder != null) {
            return false;
        }

        mCamera.unlock();

        mediaRecorder = new MediaRecorder();
        mediaRecorder.setCamera(mCamera);
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
        mediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
        mediaRecorder.setMaxDuration(15000);
        mediaRecorder.setOrientationHint(90);

        if (cameraFront) {
            mediaRecorder.setOrientationHint(270);
        }

        CamcorderProfile profile = CamcorderProfile.get(Camera.CameraInfo.CAMERA_FACING_FRONT, CamcorderProfile.QUALITY_HIGH);
        profile.fileFormat = MediaRecorder.OutputFormat.MPEG_4;
        profile.videoCodec = MediaRecorder.VideoEncoder.H264;

        // Apply to MediaRecorder
        mediaRecorder.setProfile(profile);
        mediaRecorder.setVideoEncodingBitRate(1000000);
        mediaRecorder.setOutputFile(videoFile.getAbsolutePath());
        toggleFlash.setVisibility(View.GONE);
        switchCamera.setVisibility(View.GONE);
        importMedia.setVisibility(View.GONE);
        capture.setBackgroundResource(R.drawable.take_photo_red_icon);

        countDownTimer1 = new CountDownTimer(16000, 600) {
            public void onTick(long millisUntilFinished) {
                scaleView(capture);
            }

            public void onFinish() {
                countDownTimer1.cancel();
            }
        }.start();

        countDownTimer = new CountDownTimer(16000, 1000) {
            public void onTick(long millisUntilFinished) {
                int remaining = (int) ((millisUntilFinished / 1000) % 60);
                videoTimer.setText(getString(R.string.coutdown_timer, remaining));
            }

            public void onFinish() {
                countDownTimer.cancel();
            }
        }.start();

        mediaRecorder.setOnInfoListener((mr, what, extra) -> {
            if (what == MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED) {
                releaseMediaRecorder();
            }
        });


        try {
            mediaRecorder.prepare();
        } catch (Exception e) {
            e.printStackTrace();
            releaseMediaRecorder();
            return false;
        }
        return true;
    }

    private void releaseMediaRecorder() {
        if (mediaRecorder != null) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            mediaRecorder.reset();
            mediaRecorder.release();
            mediaRecorder = null;
            mCamera.lock();
            toggleFlash.setVisibility(View.VISIBLE);
            switchCamera.setVisibility(View.VISIBLE);
            importMedia.setVisibility(View.VISIBLE);
            videoTimerLayout.setVisibility(View.GONE);
            if (animScaleMinus != null && animScalePlus != null) {
                animScaleMinus.cancel();
                animScalePlus.cancel();
            }
            countDownTimer1.cancel();
            countDownTimer.cancel();
            capture.setScaleX(1f);
            capture.setScaleY(1f);
            capture.setBackgroundResource(R.drawable.take_photoicon);

            Intent intent = new Intent(MainActivity.this, ActivityVideo.class);
            Bundle bundle = new Bundle();
            bundle.putBoolean("isCameraFront", isCameraFront());
            bundle.putString("videoFileName", videoFileName);
            bundle.putString("videoFile", videoFile.toString());
            intent.putExtras(bundle);
            startActivity(intent);
        }
    }

    public void deleteRecursive(File fileOrDirectory) {

        if (fileOrDirectory.isDirectory()) {
            for (File child : fileOrDirectory.listFiles()) {
                deleteRecursive(child);
            }
        }
        fileOrDirectory.delete();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 210 && resultCode == 1) {
            finishAffinity();
        }

        if (resultCode == RESULT_OK) {
            Uri selectedMediaUri = data.getData();

            assert selectedMediaUri != null;
            if (selectedMediaUri.toString().contains("image")
                    || selectedMediaUri.toString().contains(".jpg")
                    || selectedMediaUri.toString().contains(".jpeg")
                    || selectedMediaUri.toString().contains(".png")
            ) {
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedMediaUri);
                } catch (IOException e) { }
                Intent intent = new Intent(MainActivity.this, ActivityPicture.class);
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("isCameraFront", isCameraFront());
                    bundle.putBoolean("isUploadedFromGallery", true);
                    bundle.putString("picturePathUri", String.valueOf(selectedMediaUri));
                    intent.putExtras(bundle);
                    startActivityForResult(intent, 210);
            } else if (selectedMediaUri.toString().contains("video")
                    || selectedMediaUri.toString().contains(".mp4")
                    || selectedMediaUri.toString().contains(".3gp")
                    || selectedMediaUri.toString().contains(".avi")
            ) {
                if (this.checkVideoDuration(selectedMediaUri)) {
                    Intent intent = new Intent(MainActivity.this, ActivityVideo.class);
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("isUploadedFromGallery", true);
                    bundle.putString("videoFileName", Calendar.getInstance().getTimeInMillis() + ".mp4");
                    bundle.putString("videoFile", String.valueOf(selectedMediaUri));
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            }
        }
    }


    public boolean checkVideoDuration(Uri videoUri) {
        MediaPlayer mpl = MediaPlayer.create(this, Uri.parse(String.valueOf(videoUri)));
        int videoDuration = mpl.getDuration() / 1000;

        if (videoDuration > 15) {
            Toast toast = Toast.makeText(getApplicationContext(),
                    "Video duration should be less then 15 seconds.", Toast.LENGTH_LONG);
            toast.show();
            return false;
        }
        return true;
    }

    public void scaleView(View v) {
        animScalePlus = new ScaleAnimation(
                1f, 1.15f,
                1f, 1.15f,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        animScalePlus.setFillAfter(true);
        animScalePlus.setDuration(200);
        v.startAnimation(animScalePlus);

        animScaleMinus = new ScaleAnimation(
                1.15f, 1f,
                1.15f, 1f,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        animScaleMinus.setFillAfter(true);
        animScaleMinus.setStartOffset(300);
        animScaleMinus.setDuration(200);
        v.startAnimation(animScaleMinus);
    }
}

